package com.triodev.upstoxdemo.data

data class HoldingsResponse(
    val client_id: String,
    val `data`: List<Data>,
    val error: Any,
    val response_type: String,
    val timestamp: Long
)