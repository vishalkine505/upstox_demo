package com.triodev.upstoxdemo.di;

import android.app.Application;


import com.triodev.upstoxdemo.di.module.ActivityModule;
import com.triodev.upstoxdemo.di.module.AppModule;
import com.triodev.upstoxdemo.di.module.FragmentsModule;
import com.triodev.upstoxdemo.di.module.NetworkModule;
import com.triodev.upstoxdemo.di.module.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import dagger.android.support.DaggerApplication;

@Singleton
@Component(
    modules = {
      AndroidSupportInjectionModule.class,
      AppModule.class,
      NetworkModule.class,
      ViewModelModule.class,
      ActivityModule.class,
      FragmentsModule.class
    })
public interface AppComponent extends AndroidInjector<DaggerApplication> {
  @Component.Builder
  interface Builder {

    AppComponent build();

    @BindsInstance
    Builder application(Application application);
  }


}
