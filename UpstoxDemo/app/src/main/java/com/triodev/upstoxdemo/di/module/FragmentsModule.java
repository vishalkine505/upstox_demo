package com.triodev.upstoxdemo.di.module;

import com.triodev.upstoxdemo.ui.fragment.AtHomeFragment;
import com.triodev.upstoxdemo.ui.fragment.InStudioFragment;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class FragmentsModule {

  @ContributesAndroidInjector
  abstract AtHomeFragment contributesAtHomeFragment();

  @ContributesAndroidInjector
  abstract InStudioFragment contributesInStudioFragment();
}
