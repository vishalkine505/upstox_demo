package com.triodev.upstoxdemo.ui.viewmodels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.triodev.upstoxdemo.data.HoldingsResponse
import com.triodev.upstoxdemo.repository.HomeRepository
import javax.inject.Inject

class HomeViewModel @Inject constructor(var homeRespository: HomeRepository?) : ViewModel() {
    companion object {
        private val TAG = HomeViewModel::class.java.simpleName
    }
    val _atHomeResponse: MutableLiveData<HoldingsResponse> = MutableLiveData()

    val atHomeResponse: LiveData<HoldingsResponse>? = homeRespository?.getHomeRespone()

    fun fetchAtHome() {
         homeRespository?.fetchAtHomeData()

    }

}