package com.triodev.upstoxdemo.di.module;

import android.app.Application;
import android.content.Context;


import com.triodev.upstoxdemo.api.UpstoxService;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module
public class AppModule {

  @Provides
  @Singleton
  UpstoxService provideApiService(Retrofit retrofit) {
    return retrofit.create(UpstoxService.class);
  }

  @Provides
  Executor provideExecutor() {
    int numberOfCores = Runtime.getRuntime().availableProcessors();
    return Executors.newFixedThreadPool(numberOfCores * 2);
  }

  @Provides
  @Singleton
  Context provideContext(Application application) {
    return application;
  }
}
