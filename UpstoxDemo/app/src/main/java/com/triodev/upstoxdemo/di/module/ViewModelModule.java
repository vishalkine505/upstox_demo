package com.triodev.upstoxdemo.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;


import com.triodev.upstoxdemo.di.FitternityViewModelFactory;
import com.triodev.upstoxdemo.di.ViewModelKey;
import com.triodev.upstoxdemo.ui.viewmodels.HomeViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

  @Binds
  abstract ViewModelProvider.Factory bindViewModelFactory(FitternityViewModelFactory factory);

  @Binds
  @IntoMap
  @ViewModelKey(HomeViewModel.class)
  abstract ViewModel bindHomeViewModel(HomeViewModel homeViewModel);


}
