package com.triodev.upstoxdemo.api;


import com.triodev.upstoxdemo.data.HoldingsResponse;

import retrofit2.Call;
import retrofit2.Response;
import retrofit2.http.GET;


/** REST end points */
public interface UpstoxService {

  @GET("v3/6d0ad460-f600-47a7-b973-4a779ebbaeaf")
  Call<HoldingsResponse> getHoldings();


}
