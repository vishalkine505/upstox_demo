package com.triodev.upstoxdemo.di.module;

import com.triodev.upstoxdemo.common.BaseActivity;
import com.triodev.upstoxdemo.ui.activity.DashboardActivity;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;


@Module
public abstract class ActivityModule {

  @ContributesAndroidInjector(modules = FragmentsModule.class)
  abstract BaseActivity contributeBaseActivity();


  @ContributesAndroidInjector
  abstract DashboardActivity contributesDashboardActivity();

}
