package com.triodev.upstoxdemo.repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.demo.fitternity.extentions.onEnqueue
import com.triodev.upstoxdemo.api.UpstoxService
import com.triodev.upstoxdemo.data.HoldingsResponse
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class HomeRepository@Inject constructor(val service: UpstoxService) {
    companion object {
        private val TAG = this.javaClass.name
    }

    private val atHomeResponse: MutableLiveData<HoldingsResponse> =
        MutableLiveData()

    public fun getHomeRespone():LiveData<HoldingsResponse>{
        return atHomeResponse;
    }

     fun fetchAtHomeData():MutableLiveData<HoldingsResponse> {
        service.holdings.onEnqueue {
            onResponse ={
                Log.i(TAG,it.body().toString())
                 atHomeResponse.postValue(it.body())
            }
            onFailure ={
                Log.i(TAG,it?.message.toString())
                atHomeResponse.postValue(null)
            }
        }
        return atHomeResponse
    }



}