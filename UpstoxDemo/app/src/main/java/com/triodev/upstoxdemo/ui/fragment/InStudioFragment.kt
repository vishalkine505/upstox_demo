package com.triodev.upstoxdemo.ui.fragment


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.triodev.upstoxdemo.R
import com.triodev.upstoxdemo.databinding.FragmentInStudioBinding
import com.triodev.upstoxdemo.di.FitternityViewModelFactory
import com.triodev.upstoxdemo.ui.viewmodels.HomeViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class InStudioFragment:DaggerFragment(),SwipeRefreshLayout.OnRefreshListener {

    companion object {
        private val TAG = this.javaClass.name
    }

    private lateinit var inStudioBinding: FragmentInStudioBinding

    private lateinit var viewModel: HomeViewModel
    @Inject
    lateinit var factory: FitternityViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        inStudioBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_in_studio, container, false);
        inStudioBinding.lifecycleOwner = viewLifecycleOwner
        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)

        return inStudioBinding.root
    }

    override fun onRefresh() {
    }


}