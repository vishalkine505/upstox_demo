package com.triodev.upstoxdemo.common;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.recyclerview.widget.RecyclerView;


import org.jetbrains.annotations.NotNull;

public abstract class BaseRecyclerViewAdapter<T>
    extends RecyclerView.Adapter<BaseRecyclerViewAdapter.MyViewHolder<T>> {

  @NotNull
  public MyViewHolder<T> onCreateViewHolder(ViewGroup parent, int viewType) {
    LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
    ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);
    return new MyViewHolder<>(binding);
  }

  public void onBindViewHolder(MyViewHolder<T> holder, int position) {
    T obj = getObjForPosition(position);
    holder.bind(obj);
    holder
        .binding
        .getRoot()
        .setOnClickListener(view -> BaseRecyclerViewAdapter.this.onItemClick(obj));
  }

  @Override
  public int getItemViewType(int position) {
    return getLayoutIdForPosition(position);
  }

  protected abstract T getObjForPosition(int position);

  protected abstract int getLayoutIdForPosition(int position);

  protected abstract void onItemClick(T item);

  public static class MyViewHolder<T> extends RecyclerView.ViewHolder {
    private final ViewDataBinding binding;

    public MyViewHolder(ViewDataBinding binding) {
      super(binding.getRoot());
      this.binding = binding;
    }

    public void bind(T obj) {
      binding.setVariable(BR.obj, obj);
      binding.executePendingBindings();
    }
  }
}
