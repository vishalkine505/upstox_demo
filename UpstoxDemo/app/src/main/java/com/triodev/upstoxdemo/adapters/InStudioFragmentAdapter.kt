package com.triodev.upstoxdemo.adapters

import com.triodev.upstoxdemo.R
import com.triodev.upstoxdemo.common.BaseRecyclerViewAdapter


class InStudioFragmentAdapter : BaseRecyclerViewAdapter<String>() {


    private lateinit var inStudioList :List<String>

    override fun getLayoutIdForPosition(position: Int): Int {
       return R.layout.activity_main
    }

    override fun getItemCount(): Int {
       return inStudioList.size
    }

    override fun getObjForPosition(position: Int): String {
       return inStudioList[position]
    }

    override fun onItemClick(item: String?) {
    }

    fun setInStudioList(inStudioList: List<String>){
        this.inStudioList = inStudioList
        notifyDataSetChanged()
    }
}