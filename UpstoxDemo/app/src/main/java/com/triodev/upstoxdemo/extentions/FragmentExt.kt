package com.demo.fitternity.extentions

import android.os.Bundle
import androidx.fragment.app.Fragment

/**
 * Instantiates a fragment
 * @param lambda is a High order lambda function that should have the bundle methods
 */
inline fun <T : Fragment> T.initBundle(lambda: Bundle.() -> Unit): T = this.apply {
    arguments = Bundle().apply{lambda()}
}
