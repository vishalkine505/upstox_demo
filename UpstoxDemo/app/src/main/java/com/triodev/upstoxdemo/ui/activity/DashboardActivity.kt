package com.triodev.upstoxdemo.ui.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.ViewPager
import com.triodev.upstoxdemo.extentions.setFullScreen
import com.triodev.upstoxdemo.R
import com.triodev.upstoxdemo.adapters.CategoryPageAdapter
import com.triodev.upstoxdemo.databinding.DashboardActivityBinding
import dagger.android.support.DaggerAppCompatActivity

class DashboardActivity: DaggerAppCompatActivity()  {

    companion object {
        private val TAG = this.javaClass.name
    }

    private lateinit var homeBinding: DashboardActivityBinding
    private lateinit var adapterViewPager: CategoryPageAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        setFullScreen()
        super.onCreate(savedInstanceState)
        homeBinding =DataBindingUtil.setContentView(this, R.layout.dashboard_activity)
        setupTabs()
    }

    private fun setupTabs(){
        adapterViewPager =
            CategoryPageAdapter(
                supportFragmentManager
            )
        homeBinding.categoryViewpager.beginFakeDrag()

        homeBinding.categoryViewpager.adapter = adapterViewPager

        homeBinding.categoryViewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageSelected(position: Int) {
                Log.d(TAG, "PageSelected: $position")
                tabPageSelection(position)
            }

            override fun onPageScrollStateChanged(state: Int) {
                Log.d(TAG, "PageScrollStateChanged: $state")
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                Log.d(TAG, "PageScrollStateChanged: $position")
            }
        })

        homeBinding.inStudioTab.setOnClickListener {
            homeBinding.categoryViewpager.currentItem = 0
        }
        homeBinding.atHomeTab.setOnClickListener {
            homeBinding.categoryViewpager.currentItem = 1
        }

        homeBinding.categoryViewpager.currentItem =0
        highlightStudioTab()
    }

    private fun highlightStudioTab() {
        homeBinding.inStudioTab.setTextColor(resources.getColor(R.color.yellow))
        homeBinding.atHomeTab.setTextColor(resources.getColor(R.color.black))
        homeBinding.inStudioHighlighter.setBackgroundColor(resources.getColor(R.color.yellow))
        homeBinding.atHomeHighlighter.visibility = View.GONE
        homeBinding.inStudioHighlighter.visibility = View.VISIBLE
    }

    private fun highlightHomeTab() {
        homeBinding.inStudioTab.setTextColor(resources.getColor(R.color.black))
        homeBinding.atHomeTab.setTextColor(resources.getColor(R.color.yellow))
        homeBinding.inStudioHighlighter.visibility =  View.GONE
        homeBinding.atHomeHighlighter.visibility =  View.VISIBLE
        homeBinding.atHomeHighlighter.setBackgroundColor(resources.getColor(R.color.yellow))
    }

    fun tabPageSelection(page: Int) {
        when (page) {
            0 -> {
                highlightStudioTab()
            }
            1 -> {
                highlightHomeTab()
            }
        }
    }
}