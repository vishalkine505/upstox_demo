package com.demo.fitternity.utils

interface AnimationListener {
    fun onAnimationEnd()
}