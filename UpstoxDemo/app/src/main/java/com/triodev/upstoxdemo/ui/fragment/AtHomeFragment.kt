package com.triodev.upstoxdemo.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.triodev.upstoxdemo.R
import com.triodev.upstoxdemo.databinding.FragmentAtHomeBinding
import com.triodev.upstoxdemo.di.FitternityViewModelFactory
import com.triodev.upstoxdemo.ui.viewmodels.HomeViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject


class AtHomeFragment:DaggerFragment(),SwipeRefreshLayout.OnRefreshListener{

    companion object {
        private val TAG = this.javaClass.name
    }

    private lateinit var viewModel: HomeViewModel
    @Inject
    lateinit var factory: FitternityViewModelFactory

    private  lateinit var binding: FragmentAtHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
    binding =
        DataBindingUtil.inflate(inflater, R.layout.fragment_at_home, container, false);
        binding.lifecycleOwner = viewLifecycleOwner

        viewModel = ViewModelProvider(this, factory).get(HomeViewModel::class.java)
        viewModel.fetchAtHome()

        return binding.root
    }

    override fun onRefresh() {
        viewModel.fetchAtHome()
    }


}