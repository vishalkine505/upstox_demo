package com.triodev.upstoxdemo.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.triodev.upstoxdemo.ui.fragment.AtHomeFragment
import com.triodev.upstoxdemo.ui.fragment.InStudioFragment


class CategoryPageAdapter(
    manager: FragmentManager) : FragmentStatePagerAdapter(manager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    val statusList = listOf(
        Status(
            "AtHome"
        ),
        Status(
            "ByStudio"
        )
    )
    private val fragmentList: MutableList<Fragment> = mutableListOf()
    private lateinit var fragment: Fragment


    override fun getCount(): Int {
        return statusList.size
    }

    override fun getItem(position: Int): Fragment {
        if (position == 0) {
            fragment = InStudioFragment()
            fragmentList.add(position, fragment)
        } else {
            fragment = AtHomeFragment()
            fragmentList.add(position, fragment)
        }
        return fragment
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return statusList[position].title
    }

    data class Status(
        val title: String
    )

    companion object {
        const val TAG = "CategoryPageAdapter"
    }

}